# 1. Gestion de projet et documentation

Dans le cadre de la gestion de projet et de la rédaction d'une documentation, il existe différents programmes qui peuvent nous assister dans cette tâche parfois laborieuse.
Dans le cadre du cours, nous utiliserons le programme `git`, qui est très largement répandu dans le monde de la programmation et de l'open-source, et à juste titre.


## Installation d'un shell bash

Pour pouvoir débuter à utiliser `git`, nous devons avoir accès à un terminal.
Je ne parle pas ici de la phase mortelle d'un cancer ou d'un quai d'embarquement dans un aéroport, mais de cette chose inconnue (et potentiellement terrifiante) pour le commun des mortels:

![Insérer image d'un terminal Windows et Linux/Unix](../images/terminal.png)

Cette "chose" est pour la plupart des gens un énorme mystère, mais ne vous en faites pas, nous allons démystifier tout ça au fil du temps.
Les terminaux sous Windows et sous Linux sont différents, car ils n'utilisent pas le même langage.
Les terminaux sous Windows utilisent le "Batch", tandis que, par défaut, les terminaux sous Linux utilisent le "bash".
La différence est très faible entre les deux mots, mais le langage, lui, est un peu différent dans son utilisation
Par exemple, la commande bash `ls` (que nous verrons dans un instant) qui permet de lister le contenu d'un dossier, se nomme `dir` en Batch.


Par facilité, et puisque la majorité des personnes utilisant un terminal utilisent un langage de type bash, nous allons nous focaliser sur ce langage.
Sur Linux/Mac, comme dit plus haut, les terminaux sont par défaut en bash, donc il n'y a rien à faire. Si vraiment Linux ne vous fait pas peur, c'est la solution que je vous recommande.
Sur Windows, c'est différent. Plusieurs possibilités existent et se valent.

 - La première serait de passer par un WSL (Widows Subsystem Linux), qui n'est pas une machine virtuelle Linux, mais bien un système indépendant.
Cette fonctionnalité est disponible sur les versions récentes de Windows 10 et sur Windows 11, Normalement c'est le cas de tout le monde, sauf si vous n'aimez pas Microsoft, ou que vous êtes encore bloqués sur Windows XP.
Pour plus d'informations sur comment paramétrer la WSL, vous pouvez consulter [cette page](https://itsfoss.com/install-bash-on-windows/).
Je ne couvrirai pas cette méthode ici.
 - Une deuxième serait d'installer bash indépendamment, et tant qu'à faire, puisque nous devons installer git aussi pour les besoins du cours, autant faire d'une pierre deux coups en installant Git Bash, fourni par défaut avec l'installation de git sur Windows.
Le bash fourni est très maigre, et est limité à l'utilisation de `git`, ce qui est amplement suffisant pour nos besoins.
Si vous voulez plus de fonctionnalités, la première méthode serait plus adaptée à vos besoins.


Concernant les commandes à exécuter dans le terminal, je vais adopter la convention suivante : une commande exécutée
après `$~` est à exécuter en tant qu'utilisateur normal, il n'y a pas de manipulation supplémentaire à faire. Si la
commande est exécutée après `#~`, cela indique qu'il faut être en admin (utilisateur `root`). Pour cela, il suffit de
précéder la commande par `sudo`, par exemple

```
$~ sudo cat fichier_admin.txt
```

Si vous avez besoin d'exécuter beaucoup de commandes avec `sudo`, il devient alors utile de se connecter en tant
`root` directement. Pour cela, vous pouvez exécuter la commande `sudo -i`:

```
$~ sudo -i
[sudo] password for user: 
#~
```


## Utilisation de `git`

Maintenant que nous savons comment utiliser le terminal, il est grand temps d'apprendre à utiliser git. 
Ce merveilleux outil permet de faire ce que l'on appelle du _contrôle de version_ (Versionning Control System, ou VCS en
anglais), c'est à dire qu'il va analyser un
dossier en particulier, et tous les fichiers que vous lui direz de suivre, et va enregistrer toutes les modifications
qui y seront faites. Cela possède d'énormes avantages.

Tout d'abord, si vous travaillez seul, que ce soit sur du code, des documents Excel ou autres, vous avez déjà eu
le souci où vous travaillez sur votre document, tout fonctionne parfaitement, puis vous faites des modifications, et
plus rien ne marche. Ou alors vous avez plusieurs versions de vos documents avant et après vos changements, avec par
exemple des noms du type `document.txt`, `document2.txt`, `document2-copie.txt`, ...  Dans ce cas, git permet à n'importe
quel moment de revenir aux versions précédentes, sans perdre les dernières modifications (pour autant que vous les ayez
enregistrées dans git bien entendu).

Un autre cas où git est très utile est dans le cadre du travail collaboratif. Avant que git existe (ou d'autres VCS,
comme par exemple _subversion_), le travail collaboratif passait par mailing list. Quand un bug était trouvé, un mail
était envoyé à tout le monde, et les personnes intéressées par ce problème y répondaient, et envoyaient sur cette même
mailing list le patch à ce bug, c'est-à-dire l'ensemble des modifications à apporter aux fichiers pour que le bug soit
résolu. Heureusement pour nous, cela ne fonctionne plus comme ça !

Git reprend ces principes : lorsqu'un répertoire est suivi par git, toute modification de fichier sera détectée par ce
dernier, et il faudra alors créer un _commit_ (le mail avec le patch, si vous voulez) avec les différents changements.
Git sait quels changements ont été faits, par qui, et quand ils ont été faits. Pour que les autres personnes travaillant
sur le même projet puissent voir ces changements, ils doivent alors _soutirer_ (_pull_ en anglais) les informations sur
un serveur distant où tous ces commits ont été publiés. Ce serveur distant peut être par exemple
[GitLab](https://gitlab.com), que nous allons utiliser tout au long du cours, mais aussi [GitHub](https://github.com),
[BitBucket](https://bitbucket.org/), et encore bien d'autres.

Je ne rentrerai pas dans les détails du fonctionnement de git dans cet article, car cela n'est pas nécessaire pour notre
utilisation (sauf pour régler quelques conflits que j'aborderai dans un paragraphe un peu plus loin). Nous allons donc
apprendre les choses essentielles pour la suite, c'est-à-dire :

* Comment créer un dossier suivi par git
* Comment télécharger un projet git déjà existant
* Synchroniser ce dossier sur un serveur distant, ou _remote_ (ici GitLab)
* Comment _commit_ ses changements
* Envoyer ces changements sur le remote
* Comment mettre à jour le dossier local
* Troubleshooting

### Démarrer un nouveau projet

Ce point n'est pas essentiel pour la gestion du site web, puisque le projet est déjà existant sur GitLab. Mais si
comme moi, vous souhaitez publier vos projets déjà existants sur votre ordinateur, comme par exemple les fichiers 
OpenSCAD ou FreeCAD du module 2, alors cela peut vous intéresser.

Rendez-vous dans le dossier que vous souhaitez faire suivre par git. Imaginons que votre dossier se nomme `test`, et que
votre dossier ressemble à ceci :

```
test
├── README.md
└── test.txt
```

Une fois dedans, exécutez la commande `git init`. Cela va créer un dossier caché `.git` qui contiendra tout l'historique
des modifications et toutes les infos de git en général. La nouvelle arborescence resemble à ceci:

```
test
├── .git
│   ├── branches
│   ├── config
│   ├── description
│   ├── HEAD
│   ├── hooks
│   │   ├── applypatch-msg.sample
│   │   ├── commit-msg.sample
│   │   ├── fsmonitor-watchman.sample
│   │   ├── post-update.sample
│   │   ├── pre-applypatch.sample
│   │   ├── pre-commit.sample
│   │   ├── pre-merge-commit.sample
│   │   ├── prepare-commit-msg.sample
│   │   ├── pre-push.sample
│   │   ├── pre-rebase.sample
│   │   ├── pre-receive.sample
│   │   ├── push-to-checkout.sample
│   │   └── update.sample
│   ├── info
│   │   └── exclude
│   ├── objects
│   │   ├── info
│   │   └── pack
│   └── refs
│       ├── heads
│       └── tags
├── README.md
└── test.txt
```

Ce dossier `.git` est déjà fort chargé, mais rassurez vous, vous n'aurez pas besoin de vous y rendre, puisque toutes les
commandes de git s'en occupent pour vous !

Pour vérifier que git fonctionne bien, vous pouvez exécuter la commande `git status`:

```
$~ git status
On branch master

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	README.md
	test.txt

nothing added to commit but untracked files present (use "git add" to track)
```

Nickel ! Il y a déjà beaucoup d'informations utiles ici, nous les développerons au fur et à mesure de ce tutoriel.

Une chose importante à noter maintenant est qu'il n'y a actuellement aucun lien avec GitLab. Il faut donc créer un
repository sur GitLab, et le lier au dossier git local.

Sur la page principale de GitLab quand vous vous connectez, en haut à droite vous trouverez le bouton "New Project".
Créez alors un nouveau projet vierge, avec le nom de votre choix, et en indiquant si vous voulez le rendre public ou
non.

**Attention !** Décochez la case pour initialiser le projet avec un README, cela créera un conflit lors du premier push,
autant l'éviter et le créer directement dans votre dossier local.

<!-- Insert image -->

Une fois le projet créé, vous serez redirigé vers la page du projet. Vous trouverez sur cette page le bouton "Clone".
Copiez le lien sous "Clone with SSH", ce sera nécessaire pour la suite.

Revenons dans le terminal. Il est temps de faire votre premier commit. Avant de pouvoir suivre les changements de vos
fichiers, il faut que vous indiquiez explicitement à git quels fichiers il doit suivre. La commande `git add` sert
exactement à cela ! Elle prend comme paramètre les fichiers et/ou dossiers à suivre. Pour ajouter tous les fichiers du
répertoire, vous pouvez simplement exécuter `git add .`. Un nouveau `git status` affiche désormais quelque chose de
différent : 

```
$~ git status
On branch master

No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)
	new file:   README.md
	new file:   test.txt

```

"Changes to be committed"... Il ne reste donc plus qu'à commit ! Pour cela, c'est la commande `git commit` qu'il nous
faut. On ajoutera aussi le paramètre `-m` pour ajouter un message décrivant les modifications. Ceci est très important
pour pouvoir vous retrouver dans l'historique. Le titre du message doit pouvoir continuer la phrase "Grâce à ce commit,
je peux..." (_"Thanks to this commit, I can..._). Donc _grâce à ce commit, nous pouvons_ initialiser le dépôt (_repo_ en
anglais abrégé).

```
$~ git commit -m "Initialize repo"
[master (root-commit) b6c58de] Initialize repo
 2 files changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 README.md
 create mode 100644 test.txt
```

Commit réussi ! Avant de pouvoir envoyer les changements sur GitLab, il faut aussi informer git de la destination. Pour
cela, il faut exécuter la commande `git remote add origin <link>`, où `<link>` est le lien copié précédemment, dans
notre cas `git@gitlab.com:Oedipsos/test.git`. Pour afficher les remotes, vous pouvez exécuter `git remote -v`

```
$~ git remote add origin git@gitlab.com:Oedipsos/test.git
$~ git remote -v
origin	git@gitlab.com:Oedipsos/test.git (fetch)
origin	git@gitlab.com:Oedipsos/test.git (push)
```

Maintenant, il ne reste plus qu'à push pour la première fois, mais attention ! Il faut préciser l'option `-u` pour
pouvoir ajouter la branche locale sur le serveur distant en même temps. L'ordre des arguments est `git push <remote>
<branch>`, la seule branche existante étant dans notre cas `master` (à vérifier avec `git branch`)

```
$~ git push -u origin master
Enumerating objects: 3, done.
Counting objects: 100% (3/3), done.
Delta compression using up to 8 threads
Compressing objects: 100% (2/2), done.
Writing objects: 100% (3/3), 228 bytes | 228.00 KiB/s, done.
Total 3 (delta 0), reused 0 (delta 0), pack-reused 0
To gitlab.com:Oedipsos/test.git
 * [new branch]      master -> master
branch 'master' set up to track 'origin/master'.
```

Et voila ! Si vous rafraîchissez la page GitLab, vous verrez vos deux fichiers présents !

<!-- Insert image -->

### Si le projet existe déjà

Si le projet est déjà présent sur GitLab, tout est bien plus simple. Il vous suffit d'en créer une copie chez vous, et
puis voilà ! Un synonyme de copier est _cloner_, alors la commande à exécuter est très logiquement `git clone` ! Ajoutez
le lien du dépôt (avec SSH dans notre cas, comme avant) et exécutez la commande avec le lien en paramètre. Par exemple,
si vous voulez télécharger les sources de l'excellent jeu osu!, actuellement en développement sur GitHub, il vous suffit
d'exécuter (cela prendra un certain temps car le projet est assez gros):

```
$~ git clone git@github.com:ppy/osu.git
Cloning into 'osu'...
remote: Enumerating objects: 426953, done.
remote: Counting objects: 100% (79/79), done.
remote: Compressing objects: 100% (53/53), done.
remote: Total 426953 (delta 43), reused 45 (delta 26), pack-reused 426874
Receiving objects: 100% (426953/426953), 161.16 MiB | 2.00 MiB/s, done.
Resolving deltas: 100% (341431/341431), done.
Updating files: 100% (4461/4461), done.
```

Et voilà, vous y avez accès ! (Pour contribuer réellement, il existe une procédure détaillée dans `CONTRIBUTING.md`, si
jamais)

### Synchroniser les dossiers locaux avec le serveur

Lorsque l'on travaille en équipe, il arrive que quelqu'un publie ses modifications avec un `git push` comme nous l'avons
fait plus haut. Ces modifications sont bien entendu disponibles sur les serveurs de GitLab, mais comment avoir accès à
ces informations localement ? Pour cela, il faut exécuter `git pull`. Simulons cette modification en
modifiant un fichier directement sur GitLab.

Si vous cliquez sur le fichier `README.md`, vous verrez un bouton bleu "Open in Web IDE". Cliquez dessus, et vous serez
envoyés sur une version web de VS Code. Ajoutez du texte comme bon il vous semble, et sauvegardez avec `Ctrl+s`. Dans la
barre de gauche, vous verrez un indicateur de modification. Allez dedans, et cliquez sur "Commit & Push" (ne créez pas
de nouvelle branche). Vous aurez donc un nouveau commit ressemblant à ça

<!-- Insert Image -->

Si vous exécutez maintenant `git status` sur votre ordinateur, vous ne verrez rien puisque nous venons de créer le repo,
mais si vous exécutez `git pull`, vous verrez ceci:

```
$~ git pull
From gitlab.com:Oedipsos/test
 * [new branch]      master     -> origin/master
Updating b6c58de..f652778
Fast-forward
 README.md | 5 +++++
 1 file changed, 5 insertions(+)
```

Git nous montre que les modifications apportées sont dans le fichier `README.md`, et qu'on y a ajouté 5 lignes,
exactement comme prévu ! On peut à présent refaire les modifications que nous voulons de notre côté, et les publier à
notre tour avec l'enchaînement classique

```
$~ git add README.md
$~ git commit -m "Fix word in README.md"
$~ git push
```


### Résolution des conflits

Mais que se passerait-il si jamais quelqu'un avait modifié la même ligne que nous? Git verrait les deux modifications,
mais ne saurait pas quelle version choisir, puisque seules les personnes gérant le projet savent quelle version est la
meilleure. Git va donc rentrer dans un état de conflit, et il nous faudra alors manuellement résoudre le conflit.

Voyons ce qu'il en est dans les faits. Imaginons donc que le fichier `README.md` ait été modifié par GitLab avant le
dernier push, par exemple en changeant le mot "l'exemple" par "illustrer". Et imaginons que notre modification locale
soit changer ce même mot par "montrer". Lorsque `git push` sera exécuté, on aura le message suivant :

```
$~ git push
To gitlab.com:Oedipsos/test.git
 ! [rejected]        master -> master (non-fast-forward)
error: failed to push some refs to 'gitlab.com:Oedipsos/test.git'
hint: Updates were rejected because the tip of your current branch is behind
hint: its remote counterpart. Integrate the remote changes (e.g.
hint: 'git pull ...') before pushing again.
hint: See the 'Note about fast-forwards' in 'git push --help' for details.
```

Mhm, Cela n'a pas marché... Si on lit le message, on se rend compte que le remote est en avance sur notre branche locale,
et git nous demande d'inclure les changements du remote avant de push... Et bien suivons ce qu'il nous dit :

```
$~ git pull
Auto-merging README.md
CONFLICT (content): Merge conflict in README.md
error: could not apply e6415d9... Fix word in README.md
hint: Resolve all conflicts manually, mark them as resolved with
hint: "git add/rm <conflicted_files>", then run "git rebase --continue".
hint: You can instead skip this commit: run "git rebase --skip".
hint: To abort and get back to the state before "git rebase", run "git rebase --abort".
Could not apply e6415d9... Fix word in README.md
```

Aïe, on a fait une bêtise ? Non, pas de crainte à avoir, si on jette un oeil dans le fichier problématique, on verra ceci

```
$~ cat README.md
# test

## Titre de test

<<<<<<< HEAD
J'écris vraiment n'importe quoi juste pour illustrer.
=======
J'écris vraiment n'importe quoi juste pour montrer.
>>>>>>> e6415d9 (Fix word in README.md)
```

git a gardé les deux modifications, et nous permet de les choisir. Si par exemple on se dit que le mot "illustrer"
convient mieux à la situation, il suffit de supprimer toutes les lignes ajoutées et de garder juste la bonne version.

```
# test

## Titre de test

J'écris vraiment n'importe quoi juste pour illustrer.
```

Ensuite, on fait comme git nous a dit :

```
$~ git add README.md
$~ git rebase --continue
```

Cela va ouvrir vim par défaut pour éditer le message (Je parle de vim dans la suite de l'article, allez y jeter un oeil
si vous n'êtes pas familiers avec ce programme). Après avoir édité le message comme vous le souhaitez, sauvegardez et
quittez vim :

```
File /home/oedipsos/Documents/ULB/MA-PHYSA/PHYS-F517/test/.git/COMMIT_EDITMSG saved
[detached HEAD 391babe] Fix word in README.md
 1 file changed, 1 insertion(+), 1 deletion(-)
Successfully rebased and updated refs/heads/master.
```

La fusion (_merge_) est réussie ! Un petit coup de `git status` pour voir où on en est :

```
$~ git status
On branch master
Your branch is ahead of 'origin/master' by 1 commit.
  (use "git push" to publish your local commits)

nothing to commit, working tree clean
```

Parfait, plus qu'à `git push` pour envoyer les changements et tout est bien qui finit bien ! Vous êtes désormais parés
à utiliser git pour vos projets !

### Petit tips en plus

* `git commit -a` permet de faire un `git add` automatique de tous les fichiers modifiés. Attention cependant, car cela
  n'ajoute pas les fichiers non suivis par git. Il faudra toujours le faire manuellement pour ceux là.
* Si jamais vous voulez revenir à l'état du dossier lors du dernier commit (et donc supprimer vous éventuelles
  modifications foireuses), vous pouvez exécuter `git checkout`. Cela est irréversible !


## Markdown et MkDocs

Cette documentation est rédigée uniquement en Markdown, un langage très léger qui fonctionne par _balises_, une façon de
dire au programme qui lira le fichier Markdown (conventionnellement d'extension `.md`) de traiter le texte entre les
balises d'une façon spéciale, comme le mettre en gras, ou insérer une image à la place. Je ne couvrirai pas ici la
syntaxe du langage, mais vous pouvez trouver une référence de la syntaxe de base (et même étendue) sur 
[markdownguide.org](https://www.markdownguide.org/).

Pour écrire le site web contenant notre documentation, nous alors utiliser la bibliothèque Python 
[`mkdocs`](https://www.mkdocs.org/), qui permet de "compiler" un ensemble de fichiers Markdown en un site web, via un
fichier de configuration écrit au préalable. Ce programme étant un module python, il est possible de l'installer par
`pip`:

```
$~ python3 -m pip install mkdocs
```

Sur Fedora, `mkdocs` est également disponible en tant que programme indépendant, par son propre package manager qui est
`dnf`:

```
#~ dnf install mkdocs
```

Le template de base est déjà sur le repo GitLab, il suffit alors de se rendre dans le dossier de base et d'exécuter la
commande suivante pour construire le site.

```
$~ mkdocs build
$~ python3 -m mkdocs build    # si mkdocs n'est pas trouvé, cette version fonctionne aussi
```

S'ensuivent alors différents messages d'information, et éventuellement des avertissements pour, par exemple, des liens
d'images incorrects. Le site nouvellement construit se trouve alors dans le dossier `_site` par défaut, et il est
évidemment possible de le consulter en ouvrant le fichier `index.html` s'y trouvant dans votre navigateur favori.

#### Astuce : centrer les images en Markdown.

Par défaut, il faut savoir que les images sont alignées sur la droite si elle est plus petite que la largeur du texte (je
n'ai pas vérifié dans le cas contraire, alors on va dire que c'est pas nécessaire dans ce cas). Il est possible de
changer ce comportement pour centrer l'image dans la colonne de texte, mais il n'est pas possible d'y parvenir en
Markdown pur.

Il faut savoir qu'en réalité, Markdown n'est qu'une interface vers HTML, le langage de balisage utilisé pour les pages
web. Il est alors tout à fait concevable d'insérer du HTML dans nos documents Markdown, qui sera alors ignoré par le
parser Markdown et laissé tel quel. Dès lors, on peut définir une section qui est centrée, contenant notre image:

```html
<div align="center">
    <img alt="Texte alternatif" src="../../images/avatar-photo.jpg" width="200"></img>
</div>
```

Le rendu dans la page est alors le suivant 

<div align="center">
    <img alt="Texte alternatif" src="../../images/avatar-photo.jpg" width="200"></img>
</div>

Ce qui est le résultat souhaité. Cela ne vient pas de moi, mais de 
[ce post](https://stackoverflow.com/questions/12090472/how-do-i-center-an-image-in-the-readme-md-file-on-github)
sur StackOverflow.


## Compresser les images

Pour les images, il n'est pas nécessaire que les images soient de haute qualité. Pour économiser de la place (et
réduire les transferts de données sur Internet), il est donc plus adapter de compresser les images. Pour cela, il faut
déjà distinguer les PNG et les JPG.

Les fichiers PNG sont des formats compressés, **sans perte**. Cela signifie qu'entre une image brute et le format
`.png`, il y a juste un changement de représentation. L'image brute (Bitmap, format `.bmp` ou `.raw`) encode la couleur
de chaque pixel consécutif, prenant donc une place folle, mais identique pour deux images de même résolution. Un `.png`
encode des séquences de pixels, par exemple "Les 245 pixels suivants sont blancs, puis les 34 suivants noirs, ..." cela
permet d'éviter la répétition d'une même information. Notez bien que la compression sera d'autant plus efficace si de
grandes zones monochromes sont présentes sur l'image. C'est le format que j'ai utilisé pour des screenshots larges de
mon PC où l'image correspond bien à ce format de compression.

Si on réduit la résolution d'un PNG, sa taille peut augmenter. En effet, cela peut paraître contre toute intuition, mais
c'est en fait très logique: Si on réduit la taille de l'image, les zones monochromes seront plus petites aussi, et la
compression moins efficace. Pour mieux compresser les petites images, il est plus adapter de passer en `.jpg`.

Le format `.jpg` utilise un système de compression **avec perte**, appelé compression en vaguelettes. Je ne connais pas
les détails de cette compression, mais tout est que cela fonctionne bien pour réduire la taille, mais il faut accepter
l'apparition d'un peu de bruit et de flou sur l'image. C'est ce format là qui sera privilégié pour les petites images ou
les photos.

Pour réduire la résolution des JPG, j'utilise la commande `convert` sur mon système, outil open-source de la suite
GraphicsMagick. Ainsi, j'ai écrit ce script pour convertir mes jpg avec une largeur fixe de 800px, largement suffisante
pour le site

``` { .sh }
#!/usr/bin/bash

# Takes the supplied image as argument, makes a copy in ".original", and resizes it to 800x.

BASE=`dirname $(readlink -f $1)`

cp "$1" "$BASE/.original/$(basename $1)"
convert -resize 800x "$1" "$1"
```

Le reste du script est juste là pour garder une copie locale des fichiers originaux, et pouvoir exécuter le script de
n'importe où, et ne pas avoir de problèmes de références inexistantes.

Pour des images plus complexes, je passe sur GIMP, j'agence mes images comme je le souhaite, et je réduis la taille de
l'image à 800px de hauteur, puis je convertis en JPG (compression à 50%, en dessous je trouve qu'il y a trop de bruit).


## Défi : Utiliser vim comme unique éditeur de texte

Parmi la myriade d'éditeurs de texte possibles, et proposés dans le cadre de ce cours, tous ont leurs propres caractéristiques.
Certains possèdent une interface graphique, comme Sublime text, d'autres sont uniquement en console, comme nano ou, vous l'aurez deviné, vim. 
_vim_, qui signifie "VI iMproved", est le successeur de _vi_, un éditeur de texte assez primitif, mais également un des plus anciens puisqu'il a été crée en 1976 par Bill Joy, pour les systèmes Unix (sur lequel est basé Linux et Mac)

vim peut sembler être un choix masochiste (oui oui), mais les avantages de cet outils sont intéressants.
Tout d'abord, vi est un éditeur présent sur tous les systèmes basés Unix de nos jours, même sur des systèmes minimalistes.
Apprendre à utiliser vim revient donc savoir éditer du texte sur n'importe quel type de système Linux, ce qui est un avantage conséquent.
Un autre avantage est que vim est extensible, on peut y rajouter des fonctionnalités personnalisées via des _plugins_,crées par nous même ou bien d'autres utilisateurs les partageant (sur GitHub/Lab par exemple). Cela permet d'aller au delà de ce que nous propose vim pour être encore plus efficace dans notre workflow.
Nous installerons certains plugins pour Markdown dans cette section.

Finalement (car il faut finir avant de partir dans une éloge sans fin), étant une application console, vim est pensé pour être utiliser uniquement au clavier (bien que l'on puisse activer la souris dans les terminaux graphiques). C'est certes un inconvénient à court terme car il y a énormément de commandes à retenir, comme le montre cette [cheat sheet](https://vim.rtorr.com/) des _principales_ commandes (oui, principales, il y en a encore bien plus à découvrir).
Cela prend du temps de les apprendre, mais avec l'habitude, éditer du texte devient très rapide et optimisé.

C'est pour cela que j'ai décidé, afin de pimenter un peu l'expérience de ce cours (et surtout apprendre autre chose que git et bash, que j'utilise depuis des années déjà), d'apprendre à utilise vim de façon concrète. Je vais donc me restreindre à utiliser uniquement vim pour éditer les fichiers markdown du site, et à configurer mon éditeur dans cette optique. 


### Commandes vitales pour utiliser vim

La cheat sheet liée plus haut couvre en détail ce que je vais dire ici, mais cette section sert surtout à mettre en exergue les commandes vitales pour une personne novice n'ayant jamais utilisé vim. Je vais donc couvrir ici comment se déplacer basiquement dans un fichier, le modifier, sauvegarder ces modifications et quitter vim. Si vous voulez apprendre plus que cela, je vous conseille dans un premier temps le **vimtutor**, qui couvre la même chose que moi, mais de façon interactive, suivant la philosophie du "learn by doing". 

#### Ouvrir un fichier dans vim

La toute première opération à faire pour pouvoir éditer du texte est de lancer vim. La commande `vim` permet d'ouvrir un _buffer_ (un fichier temporaire) dans lequel vous pouvez écrire.

|![](../images/vim.png)|
|:---:|
|*Fenêtre d'accueil de vim*|

Il y a cependant une petite manipulation à faire avant d'écrire. Si vous essayez par exemple d'écrire le mot "banane", vous verrez tout d'abord que la première syllabe du mot ne sera pas écrite, mais également en bas de votre fenêtre que vous êtes en "mode Insertion". Que s'est-il passé ?

Par défaut, vim se trouve dans le **NORMAL MODE**, qui permet d'exécuter les commandes vim.
Il existe 3 modes distincts, accessibles par différentes commandes :

| Mode        |      Commande      | Description |
| ----------- |:------------------:| ----------- |
| Normal mode |       `ESC`        | Interprétation des commandes |
| Insert mode | `i`, `a`, `A`, ... | Insertion de texte. La position du curseur va dépendre de la touche appuyée. | 
| Visual mode |      `v`, `V`      | Permet la sélection de blocs de texte pour certaines commandes |

Le Visual mode est un peu plus technique, nous n'en parlerons pas plus ici. Pour commencer à insérer du texte, il faut donc passer en mode insertion, en appuyant par exemple sur `i` (notez bien que c'est un i minuscule, c'est important). Cela va permettre d'insérer du texte à la position du curseur, jusqu'à ce que l'on revienne en mode normal en appyant sur `ESC`.
Dans l'exemple ci-dessus, nous sommes rentrés en mode insertion par la commande `a`, qui permet de commencer à insérer du texte juste après le curseur. C'est pour cela que "ba" a été ignoré : nous ne pouvions pas encore écrire. 

|![](../images/insert_mode.png)|
|:---:|
|*Découverte du mode insertion*|

Maintenant que nous sommes revenus en mode normal, comment faire, par exemple, pour revenir au début du fichier et écrire une nouvelle ligne tout en haut, un titre par exemple ?
Il suffit de déplacer le curseur ! Comment faire ?

À l'heure actuelle, les claviers sont équipés de flèches directionnelles, bien que cela fonctionne pour se déplacer, ce n'est pas le plus optimisé, car nous devons quitter notre position de frappe pour aller appuyer sur les flèches, puis revenir en position d'écriture. Pour pallier à ça, par défaut, les touches de déplacement primaire sont `h`, `j`, `k` et `l`:

```
     k
  h    l   Correspondance avec les flèches directionnelles
    j
```

Notez que pour se déplacer avec ces touches, il faut être en mode normal (les flèches directionnelles sont utilisables en mode insertion également).

Vous pouvez donc revenir au début du fichier avec les touches `h` et `k`, alterner entre les modes normal et insertion, et écrire la ligne du titre :

|![](../images/moving_around.png)|
|:---:|
|*Nouveau titre, nouvelles possibilités*|

Whoaw, quel progrès ! On sait désormais tout faire (de façon rudimentaire, certes, mais on sait le faire), il ne reste plus qu'à sauvegarder ce fichier nouvellement édité !
Repassons donc en NORMAL mode, et entrons la commande suivante :

```
:wq
```

Ceci sauvegarde les modifications du fichier (`:w`) et quitte vim (`:q`). Et voilà, il ne faut techniquement pas plus de
connaissances pour savoir utiliser vim !

Bien entendu, il existe une myriade d'autres commandes reprises dans la cheat sheet en lien plus haut, permettant
d'accélérer la manipulation (déplacements de mots en mots, de ligne en ligne, recherche de mots, ...). Je vous laisse le
loisir d'expérimenter ça par vous même.

### Préparer vim pour l'édition des fichiers markdown

Je n'ai pas spécialement eu le temps de faire une grosse configuration de vim avec des plugins dédiés. J'ai cependant
écrit un petit `.vimrc` (fichier de configuration natif de vim) me permettant d'activer le correcteur orthographique
(avec dictionnaires anglais et français), le retour automatique à la ligne lorsqu'elles deviennent trop longues, et
d'autres options cosmétiques. Le voici, vous êtes libres d'en faire ce que vous voulez, vu que c'est purement
préférentiel.

```
" Disable compatibility with vi which can cause unexpected issues.

set nocompatible

" Enable type file detection. Vim will be able to try to detect the type of file in use.
filetype on

" Enable plugins and load plugin for the detected file type.
filetype plugin on

" Load an indent file for the detected file type.
filetype indent on

" Enable row numbering and current row highlight
set number
set cursorline

" Enable syntax highlighting.
syntax on

" Set shift width to 4 spaces.
set shiftwidth=4

" Set tab width to 4 columns.
set tabstop=4

" Use space characters instead of tabs.
set expandtab

" Do not save backup files.
set nobackup

" Do not let cursor scroll below or above N number of lines when scrolling.
set scrolloff=10

" Do not wrap lines. Allow long lines to extend as far as the line goes.
set nowrap

" While searching though a file incrementally highlight matching characters as you type.
set incsearch

" Ignore capital letters during search.
set ignorecase

" Override the ignorecase option if searching for capital letters.
" This will allow you to search specifically for capital letters.
set smartcase

" Show partial command you type in the last line of the screen.
set showcmd

" Show the mode you are on the last line.
set showmode

" Show matching words during a search.
set showmatch

" Use highlighting when doing a search.
set hlsearch

" Set the commands to save in history default number is 20.
set history=1000

" set text wrap
set tw=120

" Spellchecking setup, made by Gilles Castel (https://castel.dev/)
setlocal spell
set spelllang=fr,en_gb,en_us
inoremap <C-l> <c-g>u<Esc>[s1z=`]a<c-g>u
```

