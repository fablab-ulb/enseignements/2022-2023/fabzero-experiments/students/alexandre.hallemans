# 2. Conception assistée par ordinateur (CAD tools)

(Petit disclaimer avant de commencer ce module, mes notes ayant été prises en anglais, la majorité de la documentation
de ce module sera écrite en anglais, désolé d'avance pour les non anglophones. Seule la petite introduction et ce
paragraphe seront écrits en français. Comme le dit si bien ce proverbe Jedi:

<div align="center">
    Que la Flemme soit avec toi !
</div>

 
## Petit point sur les "Compliant mechanisms"

En anglais, _compliant_ signifie conforme, accommodant. Concrètement, ces mécanismes "accommodants" sont un type de
système mécanique qui, au lieu d'assurer les articulation entre les différentes parties par des pièces rigides, imposant
des contraintes rigides au systèmes, va plutôt opter pour l'utilisation de pièces flexibles. Cela donne certes un peu
plus de libertés au système, mais cela va réduire les éventuelles erreurs sur la conception des pièces, puisque la pièce
flexible va s'adapter à ces erreurs. On peut imaginer alors différents types d'articulations, ou "_flexures_", avec
leurs propres degrés de liberté (ou DOF, _degrees of freedom_):

* Pin flexure : 3 DOF (2 swinging directions + twist)
* Blade flexure : 2 DOF (twist + swing
* Notch flexure : 1 DOF (twist blocked, swing allowed)

Micro mechanical logic gates were made using compliant mechanisms.

#### What about LEGO ?

If you have rigid bricks, and try to glue them together, the precision errors should increase.
Nevertheless we achieve to build towers of 35m tall, how is that possible ?

The piece is in fact flexible -> See blog page "The secret of the LEGO brick"

We will construct the "LEGO"  flex-links set series to make our own machine.
The goal is to use a parametric construction paradigm with the selected CAD tool.

Don't forget that by printing, some errors on the scale might occur. Theoretically, one unit in CAD Tools should be one 
millimetre in the real world (without custom scale setting), but that might be slightly different from that.

**Tip** : use the printer to create a measuring tool (ruler for instance). Since the machine as an intrinsic error, use
that to make a ruler which despite being wrong in our world, will reflect the true dimensions of the printed objects
*by the machine perspective*.

## Creative Commons licenses 

When you create something (text, drawing, source, ...), no one is allowed to copy it (copyright).
Licensing your work allows others to copy it under conditions, the mandatory condition being giving credit to the owner.
The different licenses available depends on your needs and will concerning your work :

1. CC BY: Only credit has to be given
2. CC BY-SA : Credit + shared under same license (Wikipedia's default)
3. CC BY-NC : Credit + Only non-commercial uses.
4. CC BY-NC-SA : (2) + (3)
5. CC BY-ND : Credit + no modification (has to be used as it is written originally)
6. CC BY-NC-ND : (3) + (5)

How to specify the license:

- Insert a comment in the file's header.
- Write a `LICENSE.md` for the whole project.


_Source_ : [CC website](https://creativecommons.org/)

## CAD Tools

CAD tools are software used for 2D or 3D design. There are many softwares available on the market. Some proprietary ones
are

* Fusion360
* SolidWorks
* AutoCAD

As for the open-source ones, we've got 

* OpenSCAD, code oriented, the one we will use throughout this tutorial.
* FreeCAD, GUI oriented, it can be easier to learn, but personally I don't like it.

Why use open-source softwares? Nicolas told us that some company was depending on a proprietary software, and when the company owning that mentioned software went bankrupt, they were left with unsupported files, with nothing to do with those files anymore.
Depending on such software gives you less freedom. Open-source softwares are maintained by the community, and are
continuously reviewed and fixed. Everyone can open an issue if they find some bug or missing feature, and someone will
eventually implement/fix it (or just say no, that's another scenario).

To get the most out of our 3D models, we have to see the difference between raster (pixel matrices) and vector (shapes)
graphics.


### Image formats

In raster graphics, information about a picture is store as a matrix (bi- or tridimensional) of pixels (we call them
voxels for the 3D case, so yeah, Minecraft is a voxel game). While this representation is the most natural for
displaying (a computer screen is displaying each pixel one after another), the problem is that when zooming in a
picture, you will eventually see the pixels, and consider it as a "bad resolution", a loss of quality. The most common
file formats for such representation are `.png`, `.jpg`. `.gif` files are in fact a succession of images, so we can
classify them here too.

Vector graphics, on the other hand, stores the information as a global shape, such as squares or circles. This has the
advantage of enabling us to zoom as much as we would like to, and see absolutely no difference, since each path is
calculated. Therefore, this is the kind of representation we will use. Common file formats for this is `.svg`, which is
in fact a XML file containing all the objects composing the image. We could (if we really wanna do it for the sake of
perfection) edit the XML directly and remove any unnecessary information, resulting in a lighter file, but sometimes
being more accurate than a larger file (unlike the matrix representation where a larger file has a better quality).
This is also the kind of representation used for fonts (check it in a PDF, you can zoom as far as you want).


### Inkscape

Inkscape is an open-source software for vector graphics, with a Paint like interface. The corresponding Adobe software
is Illustrator. It can be used to edit 2D object fairly easily, allowing us to design pieces for engraving or cutting
for example.

<!-- TODO: insert screenshot of Inkscape UI -->

A general tip is to think in simple shapes (lines, circles, squares, ...). For instance, you can see that worldwide known logos are often made with simple shapes (example twitter logo made entirely with circle arcs, see image)

<!-- TODO: insert twitter circle diagram -->

### OpenSCAD

No need for anything since it is only a source file


## Designing our first objects : LEGO FlexLink

For the rest of this module, we will stick to OpenSCAD for the piece designing. I find that for this application, the
code version is simple than a FreeCAD project (personal preference of course, it's up to you to use whatever you want)

First of all, we will tinker two tools : one for measuring the pin radius of a LEGO, and another for measuring its pin
spacing. This follows directly from the Tip given earlier. With that, we will be able to calibrate the units in OpenSCAD
to print a FlexLink that can be clipped on a real brand LEGO.

After designing those two tools, we will create our own original FlexLink, and what better object could we find at this
time of the year, early spring, than a spring itself? How amusing! We will think about how we could make a spiral track
(aka the spring body) and attach it to our own LEGO brick model.

You will be able to find all the `.scad` files in [that GitLab repository](https://www.gitlab.com/Oedipsos/fabzero-cad/), 


### A theory of measure

#### Pin radius measurement tool

Let's begin with the pin radius measurement tool. If you are not familiar with OpenSCAD syntax, you should give a look
at the cheat sheet you can find [here](https://www.openscad.org/cheatsheet). We will start out by making our main block.
The look will be inspired by the LEGO Technic standard beam (piece ID 6028103/64290 in their catalog). We see that this
is essentially a solid block with rounded edges, with holes drilled in it. In our case, the hole diameter will be
variable to be able to compare them to actual LEGO bricks. In OpenSCAD, this might be done via creating a cube, and
adding cylinders on the edges of that cube.

We first define the size parameters of our block. We also set de minimum and maximum hole diameters for the next step.

```C
nholes = 11;                      
spacing = 10;     // spacing between hole centers

length = (nholes - 1) * spacing;
width = 9;
height = 10;

min_d = 4.5;
max_d = 5.5;
```

The length is expected to be as large as the number of holes we want to make, so we define the spacing betweens the
holes and multiply it by the numbers of holes, minus one since the first hole is technically at $x = 0$, so ti doesn't
count in the length.

Note that each line ends with a semicolon, it is needed by the compiler after each statement. I forget it all the time,
so be careful with that.

We then create the main block with those newly created parameters:

```C
union() {     // Create the main block
    cube([length, width, height]);
    
    translate([0, width/2, 0])
        cylinder(height, r=width/2);
    
    translate([length, width/2, 0])
        cylinder(height, r=width/2);
};
```

The translation of the cylinders is made to put their centers on the middle of the side, so that the perimeter connects
with the main block. We put all the object in a `union()` to glue them together. Also, note that the actual length is 
`length + width` since we added those cylinders to the main cube. Let's see how it renders, via pressing `F5`:

<div align="center">
    <img alt="Main block" src="../../images/main-block.png"></img>
</div>

To make the holes, we can simply take the difference of that block with cylinders of variable size. Let's do it for the
largest hole, for example:

```C
difference() {
    union() { ... };    // get the code from above

    translate([0, width/2, -0.01])
        cylinder(height+0.02, r = 2.4);
};
```

<div align="center">
    <img alt="Block with hole" src="../../images/one-hole.png"></img>
</div>

Nice ! We have a hole, now let's make a dozen of them ! We will make them with different radii, with a linear evolution.
We will implement it with a loop, because we can. We will also define a function for that linear radii size progression,
called a *linear interpolation* (or _lerp_ for short) :

```C
// Performs the linear interpolation from A to B, parametrised by t.
function lerp(A, B, t) = A*(1-t) + B*t;

difference() {
    union() { ... }; 

    for(l = [0 : spacing : length]) {   // Make equidistant holes with increasing radius
        translate([l, width/2, -0.01])
            cylinder(height+0.02, r = lerp(min_d, max_d, l/length)/2);
    };
};
```

<div align="center">
    <img alt="All holes" src="../../images/rtool.png"></img>
</div>

And that's it, we have a tool for measuring from 4.5 to 5.5 wide holes, by steps of 0.1 because of that parametrisation.
We could, for a fancy and aesthetic reason, add some text on the side of the beam to tell the min and max diameters.
Since the `text` object is 2D, we have to extrude it to 3D, using `linear_extrude` :

```C
difference() {
    
    ...
    
    translate([0, 0.5, height/2])  //  Not obvious, but try playing around with 
    rotate([0, 90, -90])           //  the values to see how it changes.
    linear_extrude(1)
        text(str(min_d), 4, halign="center", valign="bottom");
};
```

The `str` function converts the number as a string, which is needed as argument for `text`. We also align at the center
because we obviously want to center our text on the beam, and aligning on the bottom prevents the text to be printed on
the rounded edge. We could do the same for the other end, without forgetting to align on the top this time, and
translating it by `length`. The final result is this : 

<div align="center">
    <img alt="Final measurement tool" src="../../images/final-rtool.png"></img>
</div>


#### Pin spacing measurement tool

Let's now move to the other tool. For that, we will first make a module to make our own LEGO brick. For that, we model
the brick as a cube, with the pins/holes formed by translating the cylindrical emplacement up, by a given amount.

``` { .c title="A basic LEGO brick model" }
module brick(hholes, vholes, r=2.4, s=8, h=10, e=1.6, center=false) {
    /* Creates a block with a pin matrix of hholes x vholes with those parameters
        r :: Pin radius
        s :: Pin spacing
        h :: Block height
        e :: Pin height
        center :: If true, the block is centered on the origin
    */
    l = hholes * s;
    w = vholes * s; 
   
    translate(center ? [-l/2, -w/2, -h/2] : [0, 0, 0])
    union() {
        difference() {
            cube([l, w, h]);
            
            for(i = [0:hholes-1], j = [0:vholes-1]) {  // Holes
                translate([s * (i+0.5), s * (j+0.5), -0.01])
                    cylinder(e+0.01, r=r+0.1);   
            };
        };
        for(i = [0:hholes-1], j = [0:vholes-1]) {  // Pins
            translate([s * (i+0.5), s * (j+0.5), h-0.01])
                cylinder(e+0.01, r=r);   
        };    
    };
};
```

For the measurement tool, we will juste make 11 bricks, and glue them together by a totally experimental trick to make
them overlap each other.

``` { .c }
height = 5;
width = 9; 

nb = 11;  // number of bricks

min_s = 7.5; 
max_s = 8.5; 

union() {
    for(i = [0 : nb-1]) {
        s = lerp(min_s, max_s, i/(nb-1));
        translate([(s-i/10) * i, 0, 0])
            brick(1, 2, s=s, h=height, center=true);
    };
};
```

The result is this magnificent brick:

<div align="center">
    <img alt="Spacing measurement tool" src="../../images/stool.png"></img>
</div>

In the end, the final measurements for the pin radius is 2.5 (someone told me that it worked at the printing session, 
next module) and the pin spacing is 8.0, since the limits are 7.9 and 8.1. The `brick` module is then updated with 
those values.


### Just flexing

Finally... The time has come to create our FlexLink. As I said in the introduction, we will be making a sort of spring.
The idea is to have a base brick, and on the top of the spring we will be mounting another brick that can connect to
other LEGOs. This could be used for example as a damper for a LEGO car, or just to make things springy because it's fun.
First of all, we have to define the way we could make a spiral in OpenSCAD.


#### You spin me right round, baby right round

Mathematically speaking, in the space generated by an orthonormal basis $\{\hat{x}, \hat{y}, \hat{z}\}$, a spiral is a
curve defined by an object moving around a circle at a constant angular speed, and moving in the perpendicular direction
with constant speed. This draws the curve $\mathcal{C}$ defined by

$$
    \mathcal{C}(t) = R(\hat{x}\cos{t} + \hat{y}\sin{t}) + vt\hat{z},
$$

where $t$ is defined as the *twist* parameter of the spiral, and $R$ is the distance from the perpendicular axis (here,
$R = \sqrt{x^2 + y^2}$). $v$ can be calculated such that the spiral reaches a fixed height at the twist limit. To create
a spiral going up with a given track shape, we can then make each individual point go up along the spiral, and that
would be it.

How can we implement it in OpenSCAD ? Well, we can define a polygonal shape as a list of points, being the corners of
that specific polygon. Since we are going from a 2D shape to a 3D object, along a spiral, let's create a module called
`extrude_2dpath_along_spiral`.

Actually, let's not do this. Why ? Because someone had the same problem one day, and made a library that everyone can
use freely -- and that's why open-source is wonderful ! This library is called `BOSL`, for _Belfry OpenScad Library_,
and it is available to download on their [GitHub repository](https://github.com/revarbat/BOSL). All the instructions are
given there. Thus to use it, we have to _include_ the needed files in our project.

``` { .c }
include <BOSL/constants.scad>
use <BOSL/paths.scad>
``` 

If we take a look at the module signature, we can see how to use it

``` { .c }
// Module: extrude_2dpath_along_spiral()
// Description:
//   Takes a closed 2D polyline path, centered on the XY plane, and
//   extrudes it along a 3D spiral path of a given radius, height and twist.
// Arguments:
//   polyline = Array of points of a polyline path, to be extruded.
//   h = height of the spiral to extrude along.
//   r = radius of the spiral to extrude along.
//   twist = number of degrees of rotation to spiral up along height.
// Example:
//   poly = [[-10,0], [-3,-5], [3,-5], [10,0], [0,-30]];
//   extrude_2dpath_along_spiral(poly, h=200, r=50, twist=1080, $fn=36);
module extrude_2dpath_along_spiral(polyline, h, r, twist=360, center=undef, orient=ORIENT_Z, align=V_CENTER) {
    ...
};	
```

which is exactly we have we intended it to work ! Let's now define the various parameters for our spiral. We want it to
be as large as our base bricks, and to have a rectangular shape.

```{ .c }
// Block dimensions
lb = 24;  // x-size
wb = 8;   // y-size
hb = 5;   // z-size

// Number of turns made by the spiral
nturns = 1.75;

// Spiral thickness
sth = 3;

// Spiral track width
stw = 1.8;

// Spiral height
sh = 30;

// Spiral radius
sr = lb / 2 - stw - 0.01;

// Rectangular shape
shape = [[0, 0], [0, sth], [stw, sth], [stw, 0]];
```

We can now build our spiral, let's try it out !

```{ .c }
extrude_2dpath_along_spiral(shape, h=sh, r=sr, twist=nturns*360, $fn=100); 
```

The `$fn` parameter is used here to have a smoother spiral, despite `$fa` being more appropriate since it is an angular
motion, but yeah, so it is. If we render that portion of code, we get a beautiful spiral !

<div align="center">
    <img alt="Spiral" src="../../images/spiral.png"></img>
</div>

Let's now make a double spiral for a better structure and a symmetric look. This can be simply done by rotating the same
spiral half a turn :

```{ .c }
extrude_2dpath_along_spiral(shape, h=sh, r=sr, twist=nturns*360, $fn=100); 
rotate([0, 0, 180])
    extrude_2dpath_along_spiral(shape, h=sh, r=sr, twist=nturns*360, $fn=100); 
```

<div align="center">
    <img alt="Double the spiral, double the fun" src="../../images/double-spiral.png"></img>
</div>


At first, I wanted to make the spiral larger, and add a perpendicular fixation to the brick, but after discussing it
with the FabLab staff, this had an issue that the constraint put on that tiny fixation could break it eventually, so it
was better to fix it on the brick directly.

Al we have left is to place our bricks at the right place. For the base brick, there is no surprise, just center it in
the XY-plane. For the upper one, however, we have to be careful with the brick orientation. Since the spiral arms turn
`360 * nturns` degrees, we have to rotate the brick along the z-axis for the same amount. Therefore, we have

```{ .c }
// Base block
translate([0, 0, hb/2])
    brick(3, 1, s=wb, h=hb, center=true);

// Upper block
translate([0, 0, hb/2 + sh])
rotate([0, 0, nturns*360])
    brick(3, 1, s=wb, h=hb, center=true);
```

With a rotation of 1.75 turns, the upper brick will be rotated 90° relative to the base brick. The final result for our
custom FlexLink is shown below.

<div align="center">
    <img alt="Spiral FlexLink" src="../../images/flexlink.png"></img>
</div>

Notice how the `brick` module we made earlier is useful in our case. But we made something useless in that design. We
don't actually need the pins on the base brick, since we will not be able to connect something on the top because of the
spiral. Would it be gone, that wouldn't change anything. Moreover, we are sparing some plastic by removing them, so it
would be a great idea to remove them.

This is something that can be done easily in our `brick` design. Remember that the module is separated in two parts :
one adding cylinders on top, and another one removing cylinders on the bottom. By setting a condition, we could make any
or both of those behaviours optional, and achieve what we want. So let's implement both, so we don't have to go back to
that module anymore.

```{ .c }
module brick(hholes, vholes, r=2.5, s=8, h=10, e=1.6, center=false, pins=true, holes=true) {
    l = hholes * s;
    w = vholes * s; 
    translate(center ? [-l/2, -w/2, -h/2] : [0, 0, 0])
    union() {
        difference() {
            cube([l, w, h]);
            
            if(holes) {  // Insert a conditional here...
                for(i = [0:hholes-1], j = [0:vholes-1]) {  // Holes
                    translate([s * (i+0.5), s * (j+0.5), -0.01])
                        cylinder(e+0.01, r=r+0.01);   
                };
            };
        };
        if(pins) {   // ...and a second one here.
            for(i = [0:hholes-1], j = [0:vholes-1]) {  // Pins
                translate([s * (i+0.5), s * (j+0.5), h-0.01])
                    cylinder(e+0.01, r=r);
            };   
        };    
    };
};
```

So easy ! That's the power of modules ! By adding the keyword argument `pins=false` for the base brick, we see indeed
that the pins are gone, as expected.

<div align="center">
    <img alt="Pins removed, yay" src="../../images/flexlink-no-pins.png"></img>
</div>

Let's move onto the next part : 3D printing our newly created tools and FlexLink !

