# 3bis. Frugal science and open-source projects

Révolution numérique, initiée par l'invention du premier transistor en 1947
Passage de l'analogique au digital.

Loi de Moore : le nombre de transistors sur les circuits intégrés double chaque année
-> problème à long terme, niveau taille
-> miniaturisation, mais on atteint les limites physiques

3 révolutions simultanées
 - Communication : Téléphones analogiques vers Internet
 - Réduction : Ordinateur de la taille d'une pièce vers GSM dans la poche
 - Fabrication : Amener la programmabilité du monde virtuel dans le monde réel.

Parallèle entre la synthèse des différentes cellules par les ribosomes : Une information codée dans nos gènes est crée dans le monde réelle

Changements dans l'enseignement ?

 - permet de contribuer à la société (website, embryon pour projet futur)
 - L'enseignant peut devenir plus un coach qu'un maître
 - Esprit critique super important pour traiter l'information
 - Les étudiants peuvent discuter avec les experts partout dans le monde
 - Accès aux ressources d'autres personnes pour pousser cette base vers le haut


## Open-source 

FOSS : Free Open-source Software (free = libre, pas gratuit)
Extended to FOSH (Free Open-source Hardware) for hardware

Réduction de coûts, customisable, contrôlable, indépendance
Demande de travailler avec de outils moins finis, mais qui peuvent encore évoluer
Open /= free





