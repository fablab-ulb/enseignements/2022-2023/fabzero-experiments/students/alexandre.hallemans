# 3. Impression 3D

Le module du jour est destiné à l'impression 3D, où nous allons imprimer les pièces réalisées dans le module précédent.
C'est également maintenant que nous allons nous rendre compte de si notre pièce est imprimable ou non.
En effet, bien que nous puissions faire ce que nous voulons dans le monde CAD, la réalité peut être fort différente, et
les contraintes physiques, peuvent nuire à la bonne impression des pièces.
Voici une liste non exhaustive de facteurs:

- Les angles trop éloignés de la verticale
- Les distances à parcourir dans le vide
- Le diamètre de la buse de l'extrudeur qui limite la résolution.
- La rapidité de l'extrudeur dans certaines parties qui cause une création de filaments
- Un mauvais calibrage de la machine.
- La pièce peut se décoller lors de l'impression.
- etc.

Nous pouvons alors remarquer certains défauts dans le design du FlexLink spiral crée durant le dernier module:

* L'angle fait par la spirale même est est important, l'impression risque d'en être altérée.
* Le manque de supports, tant pour la spirale que pour la brique supérieure.

Ces points peuvent être critiques durant l'impression de la pièce, surtout pour le bloc supérieur qui ne posera qu'un
souci qu'à la fin de l'impression, que nous devrons attendre avant de voir la catastrophe arriver.

Mais avant d'imprimer la pièce, il faut arriver à passer les instructions à l'imprimante, car notre fichier `.scad` 
est bien joli, mais incompréhensible pour cette machine. Nous devons passer par un logiciel qui va prendre un objet 
`.stl` (ou `.obj`) exportable depuis OpenSCAD, et qui va découper cet objet et créer une trajectoire pour l'extrudeur 
de l'imprimante 3D, sous la forme d'un fichier Gcode (d'extension `.gcode`). Ce logiciel ce nomme un **slicer** (_to 
slice_, découper tranche par tranche), et celui que nous allons utiliser est celui de Prusa, le **PrusaSlicer**, qui est
téléchargeable directement sur [leur repository GitHub](https://github.com/prusa3d/PrusaSlicer). Puisque je suis sur 
Fedora, j'ai installé la version pour Linux avec les exécutables `.AppImage`, mais libre à vous de choisir la version 
correspondant à votre système d'exploitation (ou de le compiler depuis les sources si vous avez l'habitude).


Nous allons donc dans un premier temps, pour nous familiariser avec ce logiciel et l'impression 3d, imprimer un des 
outils de mesure que nous avons développé sur OpenSCAD la fois précédente, en l'occurrence celui pour mesurer 
l'espacement entre les pins.

## Découverte de PrusaSlicer

Une fois PrusaSlicer installé, il est temps de l'exécuter pour la première fois.

__Note__ : Pour Linux, le fichier `.AppImage` n'est pas exécutable par défaut, il ne faut pas oublier de le rendre 
exécutable, par exemple dans le terminal à coup de `chmod +x <fichier>`.

Lors de la première exécution, une fenêtre apparaît pour installer les configurations de base. Rien à signaler pour 
cette étape, vous pouvez juste vous contenter de passer au point suivant à chaque point, sauf pour celui des 
imprimantes où il faut sélectionner tous les modèles de la "Famille MK3", qui comprend le modèle des imprimantes 
utilisées au FabLab. Aussi, lors de la dernière étape, il ne faut pas oublier de sélectionner la vue en mode 
"Expert" (modifiable par la suite dans l'interface).

Vous vous retrouverez alors avec cette interface devant vous.

||
|:---:|
|![Interface d'accueil de PrusaSlicer](../images/welcome-prusa.png)|
|Interface d'accueil du logiciel PrusaSlicer, Notez le plateau correspondant à celui de la machine.|

C'est dans cette interface que l'on importera les fichiers STL.
Et puisqu'on en parle, pourquoi ne pas le faire directement ? Importons notre brique de test pour les écarts des pins
dans PrusaSlicer. Pour cela, nous pouvons nous rendre dans `File > Import > Import STL` (ou alors avec le raccourci
clavier `CTRL+i`) et sélectionner le fichier en question.

||
|:---:|
|![Import d'un fichier ".stl" dans PrusaSlicer](../images/import-stl.png)|
| Import d'un fichier `.stl` dans PrusaSlicer. |

Et hop, notre objet apparait directement sur le plateau virtuel ! À présent, comme dit précédemment, il faut le
"slicer", le découper pour avoir des instructions compréhensibles par l'imprimante. Mais avant, une vérification
s'impose... Lors de l'impression, la surface en contact avec le plateau est très importante, car c'est cette surface qui
permettra à la pièce de bien adhérer au plateau. Il faut donc orienter la pièce sur la surface la plus grande (et la
plus adéquate), avec l'option "Place on face" ![Place on face](../images/pof.png) (Raccourci `f`). L'interface
ressemblera à cela : 

||
|:---:|
| !["Place on face" interface](../images/pof-interface.png) |
| Interface de l'outil "Place on face" |

On remarque que certaines faces sont clairement inadaptés, et que au vu de la structure de la pièce, l'orientation de
base est la plus naturelle. Il n'est donc pas nécessaire de la modifier, mais cela peut s'avérer nécessaire pour
d'autres types de pièces. 

À présent, nous pouvons slicer notre objet, en cliquant sur le bouton "Slice now" en bas à droite de la fenêtre (ou en
appuyant sur `Ctrl+r` et en se rendant dans le menu "Preview" accessible en bas à gauche, ou avec `Ctrl+6`, car 
apparemment ce n'est pas automatique...). On observe alors que la pièce a été tracée par une imprimante 3D virtuelle 
suivant le même chemin que celui fourni à la machine réelle.

||
|:---:|
| ![Slicing rendering](../images/slicing.png) |
| ![peculiar layer](../images/layer-view.png) |
| Rendu du slicing, on peut observer chaque couche indépendamment et suivre le tracé de la buse. |

Avant d'exporter le G-code, nous devons changer quelques options pour l'impression. Outre la fenêtre générale du rendu
d'impression, il y a des options pour le filament, l'impression et l'imprimante. Il est également possible d'avoir les
vues Simple, Avancé et Expert. Étant donné que je n'ai remarqué leur existence bien après les impressions, toutes les
options dont je parle sont dans la vue simple. 

Concernant les options d'impression, Je suis parti sur base du preset "0.20mm QUALITY", avec plusieurs modifications.
Ici, le 0.20mm correspond à la hauteur de couche, la résolution verticale en d'autres termes. Aus plus cette hauteur est
petite, au plus la précision verticale est grande, mais le temps d'impression sera fatalement plus grand aussi. J'ai
également réduit le nombre de périmètres (c'est-à-dire le nombre de couches minimales pour délimiter les bords de la
pièce à 2, pour accélérer un tout petit peu l'impression, mais surtout car plus serait inutile, tout simplement.

Nous pouvons également modifier la façon dont le remplissage de la pièce est effectué, ainsi que sa densité, pour réduire
la quantité de matière utilisée à l'intérieur de la brique. Dans mon cas, je l'ai mise à 10%. On nous a aussi dit pendant
la formation que la régler au dessus de 25% ne changeait pas énormément la solidité de l'objet, et que ça ne servait donc
à rien de créer une pièce plus dense. De plus, la forme du remplissage peut également influer sur la rigidité de la pièce.
Si la densité est élevée, l'influence du motif devrait logiquement être plus faible.

On peut également choisir d'ajouter une "jupe" et/ou des supports à l'impression. La jupe permet d'élargir l'aire de
contact avec le plateau, et permet donc une meilleure adhérence de la pièce. J'ai opté pour cette option pour imprimer
la brique de calibration. Les supports sont utilisés pour des pièces qui ont des parties suspendues dans le vide, pour
permettre leur impression. Cependant, dans certains cas, les supports se placent très mal, et il faut alors les créer
manuellement dans le STL.

Il est possible de choisir entre un bon nombre de remplissages différents : plein, quadrillages carrés, cubiques,
hexagonaux, ... Pour ma part, je suis resté avec le motif "Gyroïde", choisi par défaut. Cela va générer des formes
sinusoïdales selon les trois axes. La solidité de ce motif est due à ces ondulations, prenez par exemple le simple carton
plume. Si on essaie de plier le carton perpendiculairement au sens des vagues, la plaque est plus solide que dans les
autres directions.

Pour les paramètres du filament, il faut bien entendu choisir le PLA, filament que nous allons utiliser pour toutes les
impressions. Le profil par défaut pour le PLA est suffisant pour nos besoins. Les paramètres à vérifier principalement
sont le diamètre du filament et les températures. Les autres options sont principalement à but statistique,

Enfin, dans les paramètres de l'imprimante, il faut vérifier principalement les paramètres de l'extrudeur. La buse
(nozzle) que nous utilisons ici au FabLab est de 0.4mm, valeur qui devrait être renseignée dans le logiciel. Si la
hauteur de couche désignait la résolution verticale, le diamètre de la buse permet de jouer sur la résolution
horizontale. 

Pour tout cela, j'ai suivi le tutoriel écrit par Axel, disponible sur le [GitLab][1] du cours. Pour les infos sur la
calibration, l'insertion du filament, etc, je vous redirige vers ce tutoriel, car je n'ai moi-même pas effectué la
manipulation, ayant utilisé la machine calibrée par Axel pour la formation (hehehe ^^').

[1]: https://gitlab.com/fablab-ulb/enseignements/fabzero/3d-print/-/blob/main/3D-printers.md


## Impression de l'outil

Il est maintenant temps d'exporter le G-code après dernière vérification, de le déplacer sur une carte SD compatible
avec l'imprimante, et d'aller imprimer la pièce. Pour lancer l'impression, il suffit de sélectionner le fichier exporté
dans l'interface de l'imprimante, et d'attendre. Évidemment, je vous conseille fortement de rester à côté de
l'imprimante pendant l'impression des premières couches, afin de pouvoir vérifier si les réglages effectués sont bons,
et dans le cas contraire pouvoir arrêter l'impression suffisamment tôt pour arranger le souci.

Dans mon cas, l'impression s'est déroulée sans souci, et voici différentes prises de la pièce en question, pendant
impression, après impression, et avec un vrai LEGO se fixant sur les pins du milieu, montrant que la bonne taille à
sélectionner est 8mm d'écart.

<div align="center">
    <img alt="Printing the measurement tool" src="../../images/printing.jpg"></img>
</div>

<div align="center">
    <img alt="And after printing." src="../../images/printed.jpg"></img>
</div>


-------------------

A vérifier avant l'impression

- Nettoyer la buse
- (Insérer le filament) si absent ou changement de filament
- Calibrer en Z pour la première couche

Avant de lancer l'impression, il faut calibrer la machine en Z. Cela peut être fait simplement en regardant la première
couche s'imprimer, si elle s'imprime correctement. Sinon, il faudra décaler l'axe Z en conséquence. Je vous renvoie vers
[cette page](https://help.prusa3d.com/fr/article/calibration-de-la-premiere-couche-i3_112364) sur la définition de 
"correctement", et le calibrage de la première couche en général.



-------------------

## Impression du FlexLink

### Étape préliminaire

Il est désormais temps d'attaquer le vif du sujet: le FlexLink créé dans le module précédent. Pour celui-ci, il faudra
absolument avoir des supports pour pouvoir imprimer la spirale et le bloc suspendu. Cependant, les supports générés
automatiquement par PrusaSlicer prennent énormément de temps à faire, et ne permettent pas du tout un bon maintien du
filament. Pour pallier à cela, j'ai ajouté manuellement des supports à ma pièce, avec des petit cure-dents à écart
régulier le long de la spirale. Voici le code SCAD générant ces cure-dents:

``` { .c }
for(th = [0:45:359]) {
    rotate([0, 0, th])
    translate([sr + stw/2, 0, 0])
        cylinder(h=sh, r=stw/2-0.01);
};
```

Les dimensions du cylindre sont telles que le diamètre corresponde à l'épaisseur de la spirale. J'ai essayé de réduire
la hauteur des cylindres pour pas que cela dépasse, mais les formules empiriques que j'ai trouvé ne fonctionnaient pas
du tout, alors j'ai laissé tomber. Cependant, il persiste encore un souci pour la création du bloc. Pour que
l'imprimante puisse faire un pont entre les deux extrémités, il faut ajouter une petite plateforme sur les bords. J'ai
donc ajouté manuellement deux petit cubes en dessous, dont la hauteur correspond à deux couches verticales dans la
résolution choisie, pour pouvoir faire le pont entre les deux bords.

``` { .c }
translate([0, sr+stw/2, sh-0.2])
    cube([wb, stw, 0.4], center=true);

mirror([0, 1, 0])
translate([0, sr+stw/2, sh-0.2])
    cube([wb, stw, 0.4], center=true);
```

De plus, j'ai dû enlever les trous dans la brique du haut. De un, c'était pas utile (sauf économie de plastique), et de
deux, cela faisait commencer une impression dans le vide, ce qui est très compliqué en général. J'ai donc modifié mon
module `brick` de la même façon que je l'ai fait pour les pins dans le module précédent.

Le résultat simulé est le suivant (le code est toujours disponible au même endroit).

<div align="center">
    <img alt="Spiral beams" src="../../images/spiral-beams.png"></img>
</div>

Pour l'impression, j'ai ajouté une jupe à la pièce, car je craignais que la pièce se décroche à la fin de l'impression,
avec une force décuplée sur la base par la verticalité de l'objet (moments de force, effet de levier, toussa toussa).
Bref, après une quarantaine de minutes d'attente, et malgré les craintes des membres du FabLab sur la possibilité
d'imprimer cet objet, la pièce est sortie quasiment comme prévue de l'imprimante.

<div align="center">
    <img alt="Spiral printed" src="../../images/spiral-printed.jpg"></img>
</div>

La première image est le résultat en sortie directe de l'imprimante. On peut voir qu'il y a un peu de plastique étiré
entre les cylindres, ce qui était plus ou moins attendu vu que l'imprimante passait très vite sur les cylindres, assez
minces. On peut également voir que la spirale est bien régulière, mais que juste avant les cylindres le plastique
commence à plomber. L'écartement des cylindres aurait donc pu être réduit, mais après ponçage, on ne voit pas la
différence. Pour la partie critique (base de la brique), on peut voir que la plateforme plombe, mais que la base de la
brique, elle, est bien construite. On ne le voit pas sur l'image, mais le premier pont sous la brique plombe légèrement.
Cela n'est pas si grave, à nouveau on peut corriger ça. Le plus important est que le haut de la brique soit bien fait.
La seconde image est le résultat après avoir coupé les cure-dents et limé toute la spirale.

Je trouve le produit final bien réussi, la pièce possède une rigidité correcte, et bouge exactement comme prévu. Le seul
petit défaut est que j'ai agrandi les trous de la base en pensant faciliter l'attache de la pièce, mais du coup il y a
un tout petit peu de jeu, et la pièce accroche moyennement bien. 

