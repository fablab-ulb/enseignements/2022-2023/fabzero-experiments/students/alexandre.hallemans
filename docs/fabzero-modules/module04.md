# 4. FabLab Tool : CNC Milling Machine

Out of all the different tools available at the FabLab ULB, I choose to work with the CNC milling machine. I actually
wanted to learn all of them, but I already work with micro-controllers for another course. And between laser cutting and
wood milling, I find the latter more useful. Let's not wait any longer and let's dive into the real stuff.


## Working with... Fusion360 ?

Until now we have worked mostly with OpenSCAD and a bit with FreeCAD (just to see how to use it). But the 2 person who
give the CNC milling formation are using an other software, called Fusion 360. This is a very famous software in the
CAD/CAM world, but a major counter-part is that it is absolutely not free and proprietary, it belongs to the AutoDesk
suite.

BUT, since I am still a student, it is totally free, yay !

The UI looks like FreeCAD, but you can immediately tell that the product looks way more finished and feature rich than
FreeCAD. Although there is still much to discover in FreeCAD, but by testing it a bit by myself, it is by far less
intuitive than the Fusion interface. So let's stick with Fusion for the rest of this article.


## First steps

After getting Fusion from the store (the Education Licence then) and installing it on your computer, you arrive on that
blank project screen.


<!-- Insert welcome page Fusion -->

Note that I am running on Windows here. Unfortunately, Fusion does not run on any Linux distribution, so we'll have to
get a Windows OS installed to go further. Since my PC has a dual boot, it's fine.

From there, you can either start a new project from scratch, or import an existing one. Since we will only care about
the manufacturing part of the software for now, we can import an existing model for testing every operation that will be
useful in Fusion.

The model was given by the teachers, so since it is not mine, I won't give a link to a file. You could try to do it by
yourself in Fusion, it is not hard to do.

The model looks like this, it is just a bunch of pockets and holes of various shape, size and depth.

<!-- Insert Image of the toy model -->


### Setup the manufacturing

Let's go first in the "MANUFACTURING" section of Fusion, in the top left dropdown list. Then, in the Setup section,
create a new setup.

Here is the most important part. this is where you define your stock, i.e. your raw piece of material, whether it's
wood, resin, plexiglas, or any other millable stuff. (The french word for stock is "brut".)
In the second tab of the pop-up window, select "Fixed box size" and enter the size of your stock.


### Create pockets

The very first thing we would like to create are pockets. Those are essentially 2D shapes that are carved into the
piece. To do this, go in the 2D menu and click on "Create 2D Pocket".

<div align="center">
    <img alt="2D Menu" src="../../images/2d-menu.png"></img>
</div>

After that click on the **bottom surface** of the pocket. This will set the bottom height right at the expected value.
For the other settings, see the image below

<div align="center">
    <img alt="Pocket settings" src="../../images/pocket-menu.png"></img>
</div>

The most important settings are

* The tool used; it is important to select the right tool for the setup. Select the previously created tool. For all the
  next objects, the tool will be selected by default.
* Uncheck "Stock to leave"; This setting is used if you want to do, for example, a finishing pass removing more
  material. This is unwanted for our case, so we disable it.
* Check "Multiple Depths"; We need to set the maximum vertical thickness (maximum roughing step) of the stock to (at
  most) half the mill diameter. This is mandatory to avoid excessive stress on the mill, that might break it apart.
  Since we are using a 6mm mill, we set it to 3mm.
  *Note*: You can use generic formulas in the window (click on the little "f" when hovering the box with the mouse). The
  formula used is `0.5 * tool_diameter`.

You see that the mill is not going straight into the material by default, but rather by following an helicoïdal path.
This behaviour is to avoid damaginf the end of the mill. Removing material from the side increases the durability of
your mill. Note that to remove all the material, the helix diameter must not be greater that your mill diameter.

If you want to make a hole instead of a pocket, you can select the bottom contour of the hole, and set a -0.5 to -1
millimeter offset for the bottom height. This will ensure that the will passes through the stock. Since we have the
spoilboard underneath, this won't damage anything.


### Create contours

After carving all the pockets in the piece, you might want to make the main contour. For that, you can select the "2D
Contour" tool in the same menu for the pockets. This time, you have to rotate the pice and select the bottom surface of
the solid. Also, don't forget to add a negative offset for the main contour, so that the piece is well cut. Here are all
the options for the contour.

<div align="center">
    <img alt="Contour settings" src="../../images/contour-menu.png"></img>
</div>

* As for the pockets, Uncheck the "Stock to Leave" option and check the "Multiple Depths" option, with the same formula
  for the maximum roughing step.
* Ramp steepness and Ramping speed; the steepness shouldn't exceed 4 degree. Increasing this parameter will make the
  attack stronger, and therefore exerts a stronger force on the mill. 2 degree is fine. For the ramping speed, we can
  leave it by default, but we may also increase it a bit (as I did) so that when the CNC makes the transition between
  ramping speed and cutting speed, it is less brutal.
* Tabs; those is crucial. If you only make your contour, you will no longer have any link to the stock: the piece will be
  free. And a free piece of wood next to a mill spinning at about 20k RPM, imagine what it could do... If you add tabs,
  the piece will be linked to the stock, and the problem is solved. Two or three tabs of thickness 2mm are more than
  enough for small objects, but you might want more of them for larger scales. Also, note that the tab thickness is
  based on the bottom height, so if you offset the bottom, add the same distance to the tabs to leave the thickness
  unchanged.


### Simulate the process

To see a simulation of any process you program, click on it in the left setup list, and click on the simulate button.
This will show you an interface with the path followed by the mill. You will have the possibility to play the process
and see in real time the mill cutting the stock. This is useful to see if the path is correct and that there are no
weird behaviour.

For example, one problem I encountered was that sometimes, the mill was following the exact same path multiple times.
The reason was that when editing the process (right click on it in the project tree, and configure), selecting the
surface did not replace the existing one, but rather it did a chain with the existing surface, so when the first surface
was done, it started the second one (which was the same). You just have to delete all the unwanted surfaces and it's
solved.

And I was able to find the problem thanks to the simulation, so always simulate your process.


### Other stuff

You can do many other processes in Fusion, I will list some of them below, but at this point, you are able to try it out
by yourself and see what it does. Good to know: if you hover a process, a small bubble will appear, explaining briefly
what the process does. You can do

- Hole drilling : For that, you have to select the hole's inside wall. This is very quick to do, but it requires to
  change the mill. This takes much more time than drilling the hole yourself, so most of the time you will prefer to
  avoid it.
- 3D things: you can do 3D pockets and contours, meaning non vertical walls are permitted with that. I've neever used
  it, but it can be used to make some spherical pieces, if you really want to.
- Many more things...


## Creation : Quaver candlestick

### SVG edit on Inkscape

As my own creation, I decided to please my girlfriend, and create a custom gift for her. Since she is studying music
related stuff, and music applied pedagogy, and also a keyboardist since 10 years, I decided to craft her a quaver-shaped
candlestick. For the non musicians here, a quaver is used for rhythm notation, and looks like this (if you don't see it,
you can scroll in the frame below.)

<iframe src="../../images/quaver-original.svg" width="100%" height="300px"></iframe>

You can find that model [here](https://upload.wikimedia.org/wikipedia/commons/0/07/8thNote.svg) under another
CC-licence, and also in the image directory of the site repo.

But the thing is... That i find that quaver ugly. Open it in Inkscape, and you will soon understand why I tell you that.
I find that the curves are not smoorth enough. It also does have some wierd angles where it shouldn't, the top has
angles where all the other parts are curved, and the stem (the pillar, if you want) isn't straight, it widens the
higher you look !

I changed all of those points, widened a bit the stem, rescaled it to my needs and it looks like this :

<iframe src="../../images/quaver-1.svg" width="100%" height="300px"></iframe>

Then, after trying a bit to insert the candle holding part, and discussing with friends -- including my girlfriend, so it
isn't a surprise anymore for her -- I decided to make the node head more circular, so that it fits better with the circular
pocket I want to make in it. It now looks like this :


<iframe src="../../images/quaver-2.svg" width="100%" height="300px"></iframe>

But now, we have another problem : Since the hole diameter has to be 40 mm, you can see that the final product will be
HUGE ! So I decided to shorten the stem by a consequent amount, about one half of the original size. I also had to
rearrange the flag points to match the new stem size. It now looks like this, see how it is cute !

<iframe src="../../images/quaver-final.svg" width="100%" height="300px"></iframe>

The head is obviously bigger than the rest of the note, but yeah, a candle has to fit in it. Did I say what type of
candle has to fit in it? No? My, my, How could I forget that ? It is meant to be tealights, that are about 38mm wide, so
the 40mm hole makes perfect sense.


### Prototyping on Fusion

Now that the SVG is done and ready to use, we can import it in Fusion. Create a sketch, and Use the "Insert SVG"
function to import the SVG we edited as a path. After extruding it, and extruded an circle on the surface to make a 10mm
deep pocket, we get this final result.

<div align="center">
    <img alt="3d model of the quaver" src="../../images/3d-quaver.png"></img>
</div>

For the manufacture, nothing spectacular. Create a pocket where there is an obvious pocket, and make the contour. Don't
forget to add the tabs to keep the object attached to the stock. The final manufacturing process path looks like this.

<div align="center">
    <img alt="Manufacturing path" src="../../images/manufacture-quaver.png"></img>
</div>

**Note**: There are only 3 tabs here. In my original design I made them automatic, and it gave me 5 tabs. It was a quite
long process to remove them, the "fast" way that damaged the piece. So I went down to three since it was enough, and I
removed them with a disc cutter and filed them down to avoid damaging the external layer of the plywood.

If we take a look in the simulation, we see that the machine will take about ten minutes to finish the piece, which is
well under fifteen minutes.

## Time to mill our quaver

When you have completed the manufacturing in Fusion, it's time to export it as G-code, the same way we did before with
the slicer when 3d-printing. At the FabLab ULB, we use a Kinetic-NC, from CNC-Step. We have to specify it to the
post-processor -- the G-code generator) -- in order to get a set of instructions compatible with the machine to avoid getting
out of bounds coordinates for example. You can also choose an appropriate name for the `.nc` file here.

<!-- Insert Post-processing window picture -->

Export it to an USB key, and open it in a notepad (or any text editor, even vim), and verify in the first lines that the
tool used has the right parameters. If it is alright, we are ready to enter the CNC Room.


### Safety measures

The CNC is a useful and wonderful tool, but it is also dangerous, be careful when manipulating it. At the FabLab ULB,
some safety mechanisms are used to avoid any injuries.

* The CNC head is controlled by the computer only, and the machine is enclosed in a plexiglas box next to it. The CNC
  cannot be used if any of the doors is open (magnetic detection). Be sure the doors are closed before starting any
  move.
* Since the doors are closed by strong magnets, when closing it you could snap your fingers in the doors, so handle it
  also with care.
* When inserting/removing any mill, wear gloves to avoid getting cut (it does). Or at least do it very carefully.
* When the machine is functionning, always stay next to it, so you can stop it if anything unexpected happens. If you
  have to leave, ask someone to stay or simply open the door. This will automatically pause the process.
* The machine is noisy, and so is the vacuum cleaner too. Wear a noise reduction headset to protect your hearing since
  you have to stay next to it.

Here is a picture of the CNC outside box.

<div align="center">
    <img alt="CNC milling machine" src="../../images/cnc.jpg"></img>
</div>


### Installing the mill

**Warning**: As I said earlier, handle the mill with care, since it can cut. Usage of gloves is recommanded.

Sometimes you will have change the mill, like I had to do because the previous mill burnt. Or maybe you will need to
because you need a special mill, or you just want to use your own.

First of all, you have to remove the vacuum extension of the CNC. Just unscrew a little bit the two screw in front of
and behind the motor unit. you can then pull it down and put it aside for now.

To change the mill, you will need two wrenches to unscrew the holder : one to hold the top of the shaft, and the other
for the shaft itself. If you hold the wrenches in a V-shape, you have to push toward the inside to unscrew the shaft.
Unscrew it manually until you cannot go further, and do the wrench thing once again. After that you can unscrew until
the end, meaning until you can safely remove the mill.

Insert the new mill, and do the exact same steps in the reverse order. Et voilà, you have done it !


### Fixing the stock under the CNC mill

Before going any further, you should fix your stock under the CNC. You will see that there is already a wooden plate
underneath. This plank is called a _spoilboard_, and it is meant to be cut, to prevent any damage on the CNC structure.
In french, it is called a "martyr". You can drill some holes in you stock's corners to screw it on the spoilboard
(unless you want to _screw_ up your whole process... Okay, I'll stop, I promise). You can find all the material in the
room.

Be careful to take the screw height and position into account when setting the clearance height and the position of your
object. If the mill collides with the screw, the mill can just break apart, and will propell metal scraps at a very high
speed, you can damage the machine itself, but also yourself or other people. Don't take the risk !

### Calibrate the CNC

When starting any session with the CNC, you should always reset the CNC calibration. When you start the CNC software
("KinetiC-NC - Program" on the computer), go in the `Jog/Setup` tab, and click on "to park position". A window should
pop up, telling you that the machine should be calibrated. Click on OK, and wait until the machine finishes. This sets
the **machine zero**, the machine point of reference.

Now it's time to set the **object zero**. Usually, you set the zero at the center of the stock, bus sometimes you will
have to set it elsewhere. It you keep it centered, and if your stock is rectangular, you can find the origin at the
diagonals intersection. If not, find a way to calibrate your zero by measuring it another way.

To set the CNC mill at the origin, you can move it from the software right above the point you found. You will see in
the same tab buttons to move the CNC. You can also move it from the numpad, with the following mapping (keys 1-9)

     . y+  z+

    x-  .  x+

     . y-  z-

You can also choose the speed at which the CNC moves, and switch between continuous and step motion to calibrate more
accurately. When it is done, click on the big zero button next to the X and Y coordinate (upper right corner) to set the
object zero.

For the Z component, you will find a metal block with a push button in the macine box (on the left). Place it underneath
the mill, and close the door. Next, go in the `Custom` tab, and double click on the "Z0-finder" macro. This will make
the CNC go down until it reaches the button. the calibration is automatic and you don't need to touch anything after
that.

If you want to move the origin position in the XY plane, you can just move it as we did before, it shouldn't
affect the Z calibration. (Not tested) 

Now the CNC is ready to work !


### Starting the milling process

Now that the CNC is calibrated and ready to use, the remaining step is to launch the procedure ! Import your `.nc` file
from the "File" menu, and go into the `Program` tab. You will see the trajectory followed by the mill, the same way as
we saw the trajectory of the extruder when we talked about 3d-printing. 

You will see some control panels for the spindel speed (rotation) or the feed (translation), coolant stuff (that we
won't use). Those might be useful if you are uncertain about the speeds that you used in Fusion, just downgrade it at a
safer level, and make it go faster if everything's okay.

The `Program` panel shows you the imported G-code, and highlights the current instruction during the process. When
importing, of course, it will highlight the first line. You have to press the play button and it will start the
manufacturing.

Now is the fun time : You will have to wait until it is finished. You can work in the meantime, I started to write this
module documentation during the printing process.

When the CNC has finished the process, you can safely move the mill away to unscrew the stock.


### Cleaning the CNC

After using the CNC, it is best to clean it for the next users. You can just use the vacuum cleaner to remove any wood
dust/scraps in the box (it is plugged outside of the box, you can just unplug it and move the tube around. If you used
your own mill, don't forget to remove it. Be careful though, because the mill will be very hot right after the
processing. Wait about five minutes before removing it, it should be safe. It also give you the time to put all the
stuff back in place.




mine, I removed them by brute force, taking a screwdriver as a chisel, but it unfortunately damaged the piece (or should
I say "expectedely" ?)

Here are some pictures of the product during the milling, after the milling, and after the manual post-processing.

<!-- Insert first piece here -->
||
|:---:|
| ![](../images/piece-processing.jpg) |
| Piece in progress, waiting for it
| ![](../images/piece-stock.jpg) |
| Right after the process, it looks amazing ! |
| ![](../images/piece-up.jpg) |
| The upper side of the piece, I really like the glossy surface |
| ![](../images/piece-down.jpg) |
| But the tab removal was... yeah, kinda bad... |


As I said earlier, I ended up using a disc cutter to detach the piece from the stock, and I filed the remaining part. I
then used some sandpaper to get a smoother result.

Here is the final picture with all the three pieces

<!-- Insert Final picture here -->





























