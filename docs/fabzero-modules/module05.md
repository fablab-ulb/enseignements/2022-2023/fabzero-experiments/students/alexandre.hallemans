# 5. Dynamique de groupe et projet final

Le module du jour est une séance concoctée par notre chère Chloé Crokart, qui était avec nous déjà pour la toute
première séance d'introduction au cours. Le but de cette séance est de créer les groupes pour les projets finaux, en
nous connectant via divers points communs trouvés durant la séance.


## Un objet, un problème

En préparation à cette séance, Denis nous a demandés à toutes et tous de prendre avec nous un objet en lien avec une
problématique qui nous intéresse. Tout le monde (ou presque) avait suivi la consigne, et ont amenés des objets divers et
variés, allant de la peau de banane au processeur d'ordinateur. Pour ma part, j'avais amené une paire de baguettes, qui
pour moi représentait plusieurs choses.

1. La frugalité de l'objet, montrant qu'il suffit de peu de choses, deux bouts de bois par exemple, pour en faire un
   outils toujours utilisé à l'heure actuelle (certes moins pratique, mais ayant l'avantage d'être très simple à
   produire. 
2. L'utilisation de ressources renouvelables, comme le bois ici, pour autant qu'il soit géré de façon durable. La
   pénurie de matières premières pour les composants électroniques par exemple montre bien qu'il faut trouver une
   solution pour revaloriser ces ressources plus rares, et si cela est possible, d'utiliser au maximum des ressources
   renouvelables.

Nous avons alors éparpillé nos objets au centre de la salle, et avons pris le temps de voir chaque objet. Ensuite, nous
sommes partis à la recherche des propriétaires des différents objets qui pouvaient correspondre à notre thématique,
histoire de pouvoir en discuter plus amplement. Au terme du temps imparti, Nous nous sommes tous retrouvés en groupes de
4-5 personnes. Je me suis donc retrouvé avec trois autres gais lurons (comme moi, enfin je crois) :

<!-- TODO : Link to their webpage -->

* [Donovan CROUSSE](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/donovan.crousse/)
* [Christophe ORY](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/christophe.ory/)
* [Louis DEVROYE](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/louis.devroye/)

Vous trouverez bien entendu leur page personnelle dans les liens fournis.


## Tour de table

Après la formation de ces groupes, nous avons dans un premier temps fait les présentations pour ceux qui ne se
connaissaient pas encore. Et même pour ceux qui se connaissaient déjà en fait, une petite présentation en plus ne fait
jamais de mal... En même temps que notre présentation, nous avons aussi expliqué le pourquoi de notre objet.

Donovan avait choisi des peaux de banane, non pas par flemme car il n'avait rien d'autre sous la main, mais bien pour illustrer
l'abondance de déchets dans notre société, et les enjeux de leur gestion. Christophe, de son côté, avait embarqué avec
lui un gobelet de café, avec un couvercle en plastique recyclé, souhaitant illustrer là également la gestion des déchets,
mais aussi le fait que l'usage des bio plastiques est aussi polluant, et ce cela ne constitue pas une solution à long
terme. Louis, pour sa part, avait en sa possession en flacon de désinfectant (j'ai pas vérifié, mais ça avait l'air en
tout cas). Son but était de dénoncer les essais des produits pharmaceutiques sur les animaux.

Avec toutes ces informations, nous étions prêts à déterminer la thématique de notre projet.


## Le thème commun 

Pour déterminer le thème commun, nous avons tous écrit notre thématique sur un post-it (celle sur laquelle nous souhaitions
personnellement travailler). Après cela, il était question d'attribuer une note d'appréciation à chacun des sujets,
Allant de "Chaud de ouf" à "Jamais de la vie je travaille là dessus" (je paraphrase, mais l'idée est là). Les différents
sujets sont dont les suivants:

1. Bien-être animal
2. Recyclage (mon idée pour la fin, totalement arbitraire)
3. [Capitalisme vert](https://fr.wikipedia.org/wiki/%C3%89co-capitalisme) (semi découverte pour ma part, j'en avais déjà
   entendu parler avant, mais je n'ai jamais cherché à en savoir plus.)
4. Crise des matières premières

||
|:---:|
| ![Échelle thématique](../images/theme-ranking.jpg) |
| Classement des différentes thématiques par l'ensemble du groupe, le vainqueur est le thème du RECYCLAGE (_clap clap clap_) |

## À ta place, je...

Maintenant que nous nous sommes tous mis d'accord sur une thématique, il est temps d'identifier des problématiques en
lien avec ce thème. Pour trouver ce sujet, nous avons utilisé une technique gardée secrète par des moines bouddhistes
depuis des siècles, la technique du "Moi, à ta place...".

Cette technique consiste à, dans un premier temps, émettre une éventuelle idée, et d'y répondre en débutant sa prise de
parole par cette expression. Par exemple,

> - Je pense que parler du tri des plastiques serait une bonne idée.
> - Moi, à ta place, je regarderais plutôt à une façon de le remplacer car ...
> - etc.

De fil en aiguille, on arrive à préciser de plus en plus les problématiques potentielles à travailler dans le groupe,
afin de faciliter la décision finale. Nous sommes arrivés à cette liste à l'issue du processus.

<div align="center">
    <img alt="Résumé de la discussion interne" src="../../images/topic-list.jpg"></img>
</div>

Il y a des idées, bonnes ou mauvaises, des délires, notamment sur la taxidermie (recyclage des cadavres d'animaux, ça
compte non ?), et des problématiques plus précises dans les accolades à la fin de la seconde colonne.

Bien entendu, cela est uniquement le résultat d'une discussion entre les quatre membres du groupe, il est possible que
nous nous soyons enfermés dans nos idées, et que nous sommes passés à côté d'autres problématiques liées, et peut-être
encore plus intéressantes à traiter. Dans cette optique, la prochaine étape est de s'ouvrir aux autres groupes.


## ... serais à l'écoute des autres.

Nous avons donc désigné un porte-parole dans notre groupe, et nous avons désigné la personne prônant la taxidermie et
les champignons, j'ai nommé Donovan. De son voyage dans les contrées étrangères, nous avons retiré une longue liste
d'idées, certaines plus cryptiques que d'autres, mais toutes liées à notre thématique. La liste en question est affichée
ci-dessous.

<div align="center">
    <img alt="Résumé après le passage dans les autres groupes" src="../../images/other-groups-list.jpg"></img>
</div>

Nous avons un peu réfléchi entre nous pour éliminer les sujets qui ne nous intéressaient pas, mais au final on a pas
noté nos décisions, donc on va dire que le travail est à refaire...


## Y'a plus qu'à...

Maintenant que la problématique est cernée (ou presque), il ne reste plus qu'à réfléchir à une solution réalisable à
notre échelle, la prototyper, et la présenter. Mais ça, ce sera pour un autre épisode...

<div align="center">
    <img alt="To be continued, see Know Your Meme" src="../../images/to-be-continued.png"></img>
</div>

-------------

## Méthodes de dynamique de groupe

Lors d'une séance ultérieure, nous avons discuté de la dynamique de groupe, plus précisément de comment se fixer des
objectifs et prendre des décisions en tant qu'un collectif d'individus. Nous avons abordé différentes méthodes pouvant
être mises en place pour fluidifier les réunions, pouvoir recueillir des avis de façon équitable et d'éviter des
conflits/tensions dans un groupe. Étant moi-même impliqué dans le bureau étudiant de ma faculté, j'utilise régulièrement
certaines de ces méthodes, et je peux témoigner de leur efficacité !

### Ordre du Jour

Pour pouvoir planifier les réunions et savoir ce qu'il va s'y passer, il est très utile de rédiger un ordre du jour de
celle-ci. L'ordre du jour, ou OJ, reprend les points abordés pendant la réunion, dans un ordre bien défini, afin de
structurer la réunion, et éviter de perdre les gens.

Par exemple, on peut structurer une réunion comme suit.

> Météo d'entrée
> Distribution des rôles
> Résumé du PV de la dernière réunion
> Présentation de L'OJ
> Sujet 1
> ... 
> Sujet N
> Divers
> Répartition des tâches
> Planification de la prochaine réunion (si nécessaire)
> Météo de sortie

Au BES (mon bureau étudiant), on rédige systématiquement un OJ, car c'est beaucoup plus pratique et on ne perd pas le
fil.

Le point "Distribution des rôles" est juste là pour clarifier les fonctions des différents membres présents : X préside
la réunion, Y rédige le PV, Z gère le temps, etc. Ainsi tout est clair et chacun sait ce qu'il a à faire.

"Mais que sont ces météo d'entrée et de sortie, Jamy?" Merci pour cette question Fred, j'allais y venir.

### Météo d'humeur

Cette technique était inconnue pour moi, bien que cela ce rapproche un peu de la séance de blabla entre nous avant que
la réunion ne commence réellement. Le principe est simplement de faire un tour de table et de demander au gens comment
ils se sentent sur le moment même. Une personne sera peut-être super enthousiaste car son dossier de candidature a été
accepté, alors qu'une autre aura passé une journée exécrable et tirera la gueule tout du long de la réunion.

Pouvoir exprimer ces différents ressentis peuvent éviter certains quiproquos du type "Pourquoi il tire la gueule, je le
fais chier avec ce que je dis?". Ceci permet donc de prévenir certaines tensions pendant la réunion.

Très bien, la météo d'entrée, c'est ok, mais la météo de sortie, à quoi sert elle ? Et bien si par exemple une décision
prise ou le comportement de quelqu'un en particulier a déplu à quelqu'un d'autre, cela peut engendrer des frustrations,
des non-dits qui peuvent aussi engendrer des tensions. Faire une autre météo en fin de réunion permet à nouveau de
s'exprimer là dessus, et de prévenir d'éventuels conflits.

Méthode toute simple, mais qui peut éviter des situations inconfortables dans lesquelles je me suis déjà retrouvé par le
passé, quand j'étais animateur Patro (les scouts, en mieux, n'en déplaise à certains). Ces non-dits ont complètement
ruiné l'ambiance et la cohésion dans l'équipe d'animation, et je me suis moi-même retrouvé dans un cas où on me
reprochait des choses, et jamais personne ne m'en avait parlé.

Bref, communiquez, c'est important.

### Méthodes de prise de décision

Concernant les façons de prendre des décisions, il existe différentes méthodes pour y arriver

**Vote à majorité simple** : Le classique, vote à main levée pour déterminer si la décision doit être prise ou non.
Classique, ne tient pas compte d'un éventuel gradient de motivation : c'est oui ou non. On peut également faire un vote
secret pour des décisions plus sensibles, comme des votes sur personnes physiques.

**Vote préférentiel** : Plutôt que d'approuver une proposition individuellement, on demande aux gens quel projet ils
préfèrent. Celle qui recueille le plus de voix est choisie.

Une méthode que j'ai découverte pendant ce cours et que j'ai déjà détaillé plus haut pour le choix de la thématique :

**Jugement majoritaire** : On assigne un jugement de valeur sur une échelle de 5, très favorable, à 1, très défavorable,
et la proposition retenue est celle qui correspond le mieux aux motivations du groupe. Cela tient par définition compte
de l'intérêt personnel pour la proposition, et peut déjà mieux convenir au groupe.

**Autres**: Consensus, Pas d'objection, Tirage au sort, ...


