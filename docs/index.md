## Foreword

Hello!

This is an example student blog for the [2022-2023 ULB class "How To Make (almost) Any Experiments Using Digital Fabrication"](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/class-website/).

Each student has a personal GitLab remote repository in the [GitLab class group](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students) in which a template of a blog is already stored. 

The GitLab software  is set to use [MkDocs](https://www.mkdocs.org/) to turn simple text files written in [Markdown](https://en.wikipedia.org/wiki/Markdown) format, into the site you are navigating.

#### Edit your blog

There are several ways to edit your blog

* by navigating your remote GitLab repository and [editing the files using the GitLab Web Editor](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#edit-a-file).
* by using [git](https://rogerdudler.github.io/git-guide/) and Command Line on your computer : cloning a local copy of your online project repository, editing the files locally and synchronizing back your local repository with the copy on the remote server.

#### Publishing your blog

Two times a week and each time you change a file, the site is rebuilt and all the changes are published in few minutes.

#### It saves history

No worries, you can't break anything, all the changes you make are saved under [Version Control](https://en.wikipedia.org/wiki/Version_control) using [GIT](https://git-scm.com/book/en/v2/Getting-Started-About-Version-Control). This means that you have all the different versions of your page saved and available all the time in the Gitlab interface.

## Mais qui est cet énergumène ?!?

![](images/avatar-photo.jpg)

Fier étudiant de master en physique, je navigue à travers les océans à surface différentiable, bravant moultes problèmes aux valeurs propres au cours de mon épopée.
Les maths sont mon quotidien, et je vois de la physique partout (help me please).
Si un jour vous retrouvez ma santé mentale, contactez-moi sur Discord (`Oedipsos#6441`), je pourrai vous répondre entre deux commits avec un meme random...

L'origine de cette photo de profil est à discuter, mais ce sera pour une prochaine fois...  
(La réalité est que je suis moche en photo, sorry)

## De mon passé à mon présent 

Mon passé est somme toute classique pour un étudiant en physique, mes études secondaires étaient axées maths et sciences.
Bon et aussi Néerlandais et grec ancien, certains diront "accident de parcours", mais j'ai beaucoup aimé.
J'ai toujours voulu faire physique, du moins depuis mes 10 ans où ma grand-mère m'a offert un téléscope...
C'était certes un jouet, mais il m'a permis de commencer à contempler ce qui se trouvait au dessus de nos têtes à ce moment là : la Lune.
Mon père mit alors les mains sur mes épaules, et me dit : "Fiston, c'est la lampe du salon, la lune elle est dehors."
Et effectivement, tout était plus beau à l'xtérieur, tellement beau que j'ai voulu en faire mon métier : rechercher comment le monde fonctionne là haut, et je pensais trouver la réponse dans la physique...
Et c'est le cas, et j'ai même appris beaucoup plus.

Aujourd'hui, ce téléscpoe est rangé dans mes armoires, et j'en possède un bien meilleur, que j'espère améliorer au fil du temps avec, dans un premier temps, le matériel de photographie nécéssaire pour prendre de belles photos, et éventuellement fabriquer certaines pièces avec les compétences nouvellement acquises dans ce cours !

Outre la physique et l'astronomie, un autre de mes centres d'intérêts est l'informatique et la programmation, depuis mes 16 ans.
J'ai commencé par apprendre le C, car je suis masochiste, vraiment.
Puis j'ai appris Python, langage que j'apprécie beaucoup et qui reste mon langage principal dans un grand nombre de situations.
Je m'intéresse également à la programmation fonctionnelle, par le langage Haskell.

Plus récemment, j'ai découvert Julia, qui est un langage alliant rapidité comme C/Fortran et lisibilité comme Python.
Très prometteur si je continue dans la recherche.

Sinon à part ça, weeb classique, j'aime énormément l'animation japonaise et les jeux de rythme (et les jeux tout court en fait).

## Previous work

Si vous voulez voir les projets dans lesquels j'ai été impliqué, vous pouvez voir mon compte [GitHub](https://github.com/Oedipsos) qui référence tous ces projets.
J'ai aussi pour ambition de rejoindre réellement le projet [osu!lazer](https://github.com/ppy/osu), mais j'ai vraiment (vraiment) la flemme d'apprendre C#.
